# -*- coding: utf-8 -*-

#############################
# Module:   def_fLA
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fLA (ratio of land area on
#              carbonaceous rock compared to the present value).
#
###############################
from DependentVariable import *

class def_fLA(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fLA (ratio of land area on
        carbonaceous rock compared to the present value).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step: 
        fA = variables.fA.getValue(variables, time)
        fL = variables.fL.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fA * fL