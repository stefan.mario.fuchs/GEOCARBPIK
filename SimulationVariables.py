# -*- coding: utf-8 -*-

#############################
# Module:   SimulationVariables
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: All simulation variables are added, deleted or edited in the 
#              following 'variables' object. On the left side the name of the 
#              variable is mentioned and on the right side the value.
#              The variable type is not defined at this point, but in the 
#              variable definition file itself. See the user manual for 
#              further details.
#
###############################
import os
from   InputVariable import *

# Imports all def_-files in the directory:
fileList = os.listdir()
for file in fileList:
    if('def_' in file):
        module = file.split('.py')[0]
        exec("from " + module + " import *")

        
        

        
variables = { 
             
             # INPUT VARIABLES                                              
             # • Time-independent input variables
             'Ç'               : 0.75,          # validation: 0.75,            # relative proportion of carbonates in deep sea sediments
             'kWC'             : 0.00267,       # validation: 0.00267,         # rate constant for weathering of carbonates
             'kWG'             : 0.003,         # validation: 0.003,           # rate constant for weathering of organic matter
             'CO2_0'           : 300,           # validation: 300,             # present value of atmospheric CO2 (see below)
             'Fmc_0'           : 6.670E+18,     # validation: 6.670E+18,       # present value of Fmc (see below)
             'Fmg_0'           : 1.250E+18,     # validation: 1.250E+18,       # present value of Fmg (see below)
             'Fwsi_0'          : 6.670E+18,     # validation: 6.670E+18,       # present value of Fwsi (see below)
             'T_0'             : 12.4,          # validation: 12.4,            # present value of global mean surface air temperature 
             'L'               : 2,             # validation: 2,               # response of global river runoff for carbonates due to changes in global mean temperature
             
             # • Time-dependent input variables
             'fA'              : 'fA.csv',      # validation: 1.0,             # ratio of land area (t) to land area (0)
             'fD'              : 'fD.csv',      # validation: 1.12,            # ratio of river runoff (t) to river runoff (0)
             'fL'              : 'fL.csv',      # validation: 0.63,            # ratio of [carb/total land (t)] to [carb/total land (0)]
             'fSR'             : 'fSR.csv',     # validation: 1.0,             # ratio of seafloor spreading rate (t) to seafloor spreading rate (0)
             'δbc'             : 'δbc.csv',     # validation: -0.0008,         # δ13C value for carbonates being buried at time t (≈ shallow oceanic value)
             'αc'              : 'αc.csv',      # validation: 0.028,           # carbon isotope fractionation factor between organic matter and carbonates during burial
             'FERT'            : 0.4,           # validation: 0.4,             # exponent reflecting the proportion of plants globally that are fertilized by increasing CO2 and that accelerate mineral weathering
             'τ'               : 4.3,           # validation: 4.3,             # response of global mean temperature to changes in atmospheric CO2 due to the atmospheric greenhouse effect
             'Ws'              : 8.7,           # validation: 8.7,             # response on global mean temperature to changes in solar radiation over geological time
             'T'               : 'T.csv',       # validation: 12.0,            # global mean temperature
             'ACT'             : 0.09,          # validation: 0.09,            # E/RT2 = coefficient expressing the effect of mineral dissolution activation energy E on weathering rate (R = gas constant)
             'RUNfBs'          : 'RUNfBs.csv',  # validation: 0.025,           # response of global river runoff for silicates due to changes in global mean temperature
             'RUNfBBc'         : 0.087,         # validation: 0.087,           # response of global river runoff for carbonates due to changes in global mean temperature 
#             'Sr'              : 'Sr.csv',      # validation: 90,             # oceanic value of Sr87/Sr86
             'Sr'              : 'Sr (GEOCARBPIK).csv',                        # oceanic value of Sr87/Sr86
             'MSr'             : 1.25E+17,      # validation: 1.25E+17,        # mass of total SR in the oceans        
             'Rriv'            : 0.711,         # validation: 0.711,           # average Sr87/Sr86 of rivers
             'Friv'            : 3.37E+16,      # validation: 3.37E+16,        # flux of total Sr to oceans from rivers
             'Rbas'            : 0.703,         # validation: 0.703,           # average Sr87/Sr86 of basalt
             'Fbas_0'          : 9.2E+15,       # validation: 9.2E+15,         # present value of Fbas (see below)             
             'fE'              : None,                                         # biological weathering factor
             
             
             # DEPENDENT VARIABLES
             # • Steady-state independent variables
             'GEOG'            : None,                                         # effect of changes in paleogeography on temperature
             'fAD'             : None,                                         # ratio of river discharge (t) to river discharge (0)
             'fLA'             : None,                                         # ratio of carbonate land area (t) to carbonate land area (0)
             'fG'              : None,                                         # ratio of global degassing rate (t) to global degassing rate (0)
             'fC'              : None,                                         # carbonates degassing rate
             'fR'              : None,                                         # ratio of mean land relief (t) to mean land relief (0)
             'Fwg'             : None,                                         # rate of release of carbon to the ocean, atmosphere and biosphere system via the weathering of organic matter
             'Fmc'             : None,                                         # rate of degassing release of carbon to the ocean, atmosphere and biosphere system via the metamorphic, volcanic and diagenetic breakdown of carbonates
             'Fmg'             : None,                                         # rate of degassing release of carbon to the ocean, atmosphere and biosphere system via the metamorphic, volcanic and diagenetic breakdown of organic matter
             'Fbas'            : None,                                         # flux of total Sr between basalt and seawater
             'Rocm'            : None,                                         # measured average ratio of 87Sr/86Sr of seawater
             'f_CO2'           : None,                                         # feedback factor expressing the dependence of weathering on CO2
             'TDiff'           : None,                                         # ratio of global degassing rate (t) to global degassing rate (0)
             'fBs_T'           : None,                                         # feedback factor expressing the dependence of weathering on temperature for silicates
             'fBBc_T'          : None,                                         # feedback factor expressing the dependence of weathering on temperature for carbonates
             'fBBc'            : None,                                         # feedback factor expressing the dependence of weathering on temperature and on CO2 for carbonates
             'Fwc'             : None,                                         # rate of release of carbon to the ocean, atmosphere and biosphere system via the weathering of carbonates                    
             'Fbc'             : None,                                         # burial rate of carbon as carbonates in sediments
             'Fbg'             : None,                                         # burial rate of carbon as organic matter in sediments
             'Fwsi'            : None,                                         # rate of uptake of CO2 via the weathering of Ca and Mg silicates followed by precipitation of Ca and Mg as carbonates (Ebelmen-Urey reaction)
             'fBs'             : None,                                         # feedback factor expressing the dependence of weathering on temperature and on CO2 for silicates
             'fBs_RCO2'        : None,                                         # feedback factor expressing the dependence of weathering on temperature and on CO2 for silicates
             
             # • Steady-state variables
             'RCO2'            : 1.0,           # validation: 1.0,             # ratio of mass of atmospheric CO2 (t) to mass of atmospheric CO2 (0)
#             'RCO2_reference'  : 'RCO2_reference (GEOCARBIII).csv',            # reference curve
             'RCO2_reference'  : 'RCO2_reference (GEOCARBPIK).csv',            # reference curve
#             'RCO2_reference'  : 'RCO2_reference (Validation run).csv',        # reference curve
    
             # • Steady-state dependent variables
             'CO2'             : None,                                         # mass of atmospheric CO2
             'GMT'             : None,                                         # global mean surface air temperature
        
             
             # STATE VARIABLES
             # • State variables
             'C'               : 6.000E+21,     # validation: 6.000E+21,       # carbon in carbonates
             'G'               : 1.250E+21,     # validation: 1.250E+21,       # carbon in organic matter
             'δc'              : 0.003,         # validation: 0.003,           # ratio of ∆13C isotopes in carbonates
             'δg'              : -0.027,        # validation: -0.027,          # ratio of ∆13C isotopes in organic matter
             'Roc'             : 0.709,         # validation: 0.709,           # calculated average ratio of 87Sr/86Sr of oceans
             
             # • Change rates
             'dC'              : None,                                         # change of carbon in carbonates
             'dG'              : None,                                         # change of carbon in organic matter
             'δcC'             : None,                                         # value of ∆13C isotopes in carbonates (C * δc)
             'dδcC'            : None,                                         # change of value of ∆13C isotopes in carbonates
             'δgG'             : None,                                         # value of ∆13C isotopes in organic matter (G * δg)
             'dδgG'            : None,                                         # change of value of ∆13C isotopes in organic matter
             'dRoc'            : None,                                         # change of average ratio of 87Sr/86Sr of oceans
             
            }
            

            
            
            
class Variables:

    # CONSTRUCTOR
    def __init__(self, settings):
        
        # Initializes variables
        for variable in variables.keys():
            value = variables[variable]
            # Load dependent and state variable definitions
            try:
                exec('self._%s = def_%s(settings, value)' % (variable, variable))
            # All others are InputVariables
            except:
                exec('self._%s = InputVariable(settings, value)' % (variable))
        
        stateVariables     = []
        dependentVariables = []

        for attributeName in dir(self):
            attribute = getattr(self, attributeName)
            if(not attributeName.startswith("_") and isinstance(attribute, DependentVariable)):
                dependentVariables.append(attributeName)
            if(not attributeName.startswith("_") and isinstance(attribute, StateVariable)):
                stateVariables.append(attributeName)
                
        self._dependentVariables = dependentVariables
        self._stateVariables     = stateVariables

    # DEFINES GETTER AND PROPERTIES FOR EACH VARIABLE
    for variable in variables.keys():
        value = variables[variable]
        # • Getter
        exec("def get_%s(self): return self._%s" % (variable, variable))
        # • Property
        exec('%s = property(get_%s, None, None, "%s property.")' % (variable, variable, variable))