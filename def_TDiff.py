# -*- coding: utf-8 -*-

#############################
# Module:   def_TDiff
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable TDiff (global mean 
#              temperature difference).
#
###############################
import math
from DependentVariable import *

class def_TDiff(DependentVariable):
    """ Description:
    
        Definition of the dependent variable TDiff (global mean 
        temperature difference).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        τ    = variables.τ.getValue(variables,    time)
        RCO2 = variables.RCO2.getValue(variables, time)
        Ws   = variables.Ws.getValue(variables,   time)
        GEOG = variables.GEOG.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = τ * math.log(RCO2) - Ws * (abs(time)/570) + GEOG