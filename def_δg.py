# -*- coding: utf-8 -*-

#############################
# Module:   def_δg
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the state variable δg (δ13C ratio in organic 
#              material).
#
###############################
from StateVariable import *

class def_δg(StateVariable):
    """ Description:
    
        Definition of the state variable δg (δ13C ratio in organic material).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        δgG  = variables.δgG.getValue(variables,  time)
        dδgG = variables.dδgG.getValue(variables, time)
        G    = variables.G.getValue(variables,   (time + settings.stepSize) )
        
        # Perform the calculation and save the result in the variable:
        values[index+1] = (δgG + dδgG) / G