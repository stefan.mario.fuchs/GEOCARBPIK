# -*- coding: utf-8 -*-

#############################
# Module:   def_fE
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fE (biological weathering 
#              factor).
#
###############################
from scipy import interpolate
from DependentVariable import *

class def_fE(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fE (biological weathering factor).
        
    """
    # constructor
    ...

    # getter
    ...

    # setter
    ...

    # deleter
    ...
    
    # property(fget=None, fset=None, fdel=None, doc=None)
    ...
    
    # methods
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        
        beforeVascularPlants = settings.beforeVascularPlants
        withVascularPlants   = settings.withVascularPlants
        beforeAngiosperms    = settings.beforeAngiosperms
        withAngiosperms      = settings.withAngiosperms
        
        nonVascularPlants    = 0.25
        vascularPlants       = 0.875        
        angiosperms          = 1.00
        
        
        # Time of non-vascular plants
        if  (time < beforeVascularPlants):
            fE = nonVascularPlants
        # First appearance of vascular land plants
        elif(time < withVascularPlants):
            # Linear growth
            f  = interpolate.interp1d([abs(beforeVascularPlants), abs(withVascularPlants)+1], [nonVascularPlants, vascularPlants], kind='linear')
            fE = f( abs(time) )
        # Time with vascular land plants
        elif(time < beforeAngiosperms):
            fE = vascularPlants
        # First appearance of angiosperms
        elif(time <  withAngiosperms):
            # Linear growth
            f  = interpolate.interp1d([abs(beforeAngiosperms),    abs(withAngiosperms)+1], [vascularPlants, angiosperms], kind='linear')
            fE = f( abs(time) )
        # Time with angiosperms
        elif(time <= 0):
            fE = angiosperms
        
            
        values[index] = fE
#        values[index] = 1.0