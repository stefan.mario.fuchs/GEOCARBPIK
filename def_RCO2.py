﻿# -*- coding: utf-8 -*-

#############################
# Module:   def_RCO2
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable RCO2 (ratio of the CO2 
#              concentration in the atmosphere compared to the present value).
#
###############################
import math
from DependentVariable import *

class def_RCO2(DependentVariable):
    """ Description:
    
        Definition of the dependent variable RCO2 (ratio of the CO2 
        concentration in the atmosphere compared to the present value).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def getValue(self, variables, time):
        values = self._values
        settings = self._settings
        index = settings.getIndexByTime(time)
        
        if(values[index] is None):
            values[index] = self.getValue(variables, (time - settings.stepSize))
        
        return values[index]
    
    def calculation(self, variables, time):
        values = self._values
        settings = self._settings
        index = settings.getIndexByTime(time)
        
        
        beforeVascularPlants = settings.beforeVascularPlants
        withVascularPlants   = settings.withVascularPlants
        
        
        # Time of non-vascular plants
        if  (time < beforeVascularPlants):
            RCO2 = self.a(variables, time, 3)
        # First appearance of vascular land plants
        elif(time < withVascularPlants):
            RCO2 = self.b(variables, time, abs(beforeVascularPlants), abs(withVascularPlants)+1 )
        # Time with vascular land plants
        elif(time <= 0):
            RCO2 = self.c(variables, time, 3)
        
            
        values[index] = RCO2





    def a(self, variables, time, n):
        fBs     = variables.fBs.getValue(variables,      time)
        RCO2    = self.getValue(variables,              time)
        ACT     = variables.ACT.getValue(variables,     time)
        τ       = variables.τ.getValue(variables,       time)
        RUNfBs  = variables.RUNfBs.getValue(variables,       time)
        Ws      = variables.Ws.getValue(variables,       time)
        GEOG    = variables.GEOG.getValue(variables,       time)
        
        for i in (1,n):
            fBs_RCO2 = math.pow(RCO2, (0.5 + ACT * τ))                                                       * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), 0.65)  * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            W        = ((0.5 + ACT * τ) * math.pow(RCO2, (-0.5 + ACT * τ)))                                  * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), 0.65)  * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            V        = math.pow(RCO2, (0.5 + ACT * τ)) * 0.65          * (RUNfBs * τ / RCO2)                 * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), -0.35) * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            fPBS     = W + V
            RCO2     = RCO2-((fBs_RCO2 - fBs)/fPBS)
        return RCO2
  
        
        
    def b(self, variables, time, begin, end):
        begin    = abs(begin)
        end      = abs(end)
        duration = begin - end
        
        a = self.a(variables, time, 2)
        c = self.c(variables, time, 4)
        
        time    = abs(time)
        weightA = abs(time-end ) /duration
        weightC = abs(time-begin)/duration
        
        RCO2 = weightA * a + weightC * c
        
        return RCO2
        
        
        
    def c(self, variables, time, n):
        fBs     = variables.fBs.getValue(variables,      time)
        RCO2    = self.getValue(variables,              time)
        ACT     = variables.ACT.getValue(variables,     time)
        τ       = variables.τ.getValue(variables,       time)
        RUNfBs  = variables.RUNfBs.getValue(variables,       time)
        Ws      = variables.Ws.getValue(variables,       time)
        GEOG    = variables.GEOG.getValue(variables,       time)
        FERT    = variables.FERT.getValue(variables,              time)
            
        for i in (1,n):
            fBs_RCO2 = math.pow(2, FERT) * math.pow(RCO2, (FERT + ACT * τ))     * math.pow((1 + RCO2), -FERT)                                        * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), 0.65)  * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            W        = (FERT + ACT * τ) * math.pow(2, FERT) * math.pow(RCO2, (FERT + ACT * τ - 1)) * math.pow((1 + RCO2), -FERT)                     * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), 0.65)  * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            V        =                    math.pow(2, FERT) * math.pow(RCO2, (FERT + ACT * τ))     * (-FERT * math.pow((1 + RCO2), -(1 + FERT)))     * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), 0.65)  * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            X        = 0.65 * (1.32 *     math.pow(RCO2, (FERT + ACT * τ)))                        * math.pow((1 + RCO2), -FERT) * (RUNfBs * τ/RCO2) * math.pow((1 + RUNfBs * τ * math.log(RCO2) - RUNfBs * Ws * (abs(time)/570) + RUNfBs * GEOG), -0.35) * math.exp(-ACT * Ws * (abs(time)/570)) * math.exp(ACT * GEOG)
            fPBS     = W + V + X
            RCO2     = RCO2-((fBs_RCO2 - fBs)/fPBS)
        return RCO2