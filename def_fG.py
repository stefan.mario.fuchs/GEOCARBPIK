# -*- coding: utf-8 -*-

#############################
# Module:   def_fG
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fG (global degassing rate).
#
###############################
from DependentVariable import *

class def_fG(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fG (global degassing rate).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        fSR = variables.fSR.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fSR
#        values[index] = 1.0