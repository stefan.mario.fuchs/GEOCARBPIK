# -*- coding: utf-8 -*-

#############################
# Module:   def_Fmc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fmc (degassing of carbon 
#              into the atmosphere, oceans and biosphere by metamorphic, 
#              volcanic and diagenetic decomposition of carbonates).
#
###############################
from DependentVariable import *

class def_Fmc(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fmc (degassing of carbon 
        into the atmosphere, oceans and biosphere by metamorphic, 
        volcanic and diagenetic decomposition of carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        fG    = variables.fG.getValue(variables,    time)
        fC    = variables.fC.getValue(variables,    time)
        Fmc_0 = variables.Fmc_0.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fG * fC * Fmc_0