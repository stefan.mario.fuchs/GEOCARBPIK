# -*- coding: utf-8 -*-

#############################
# Module:   def_fC
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fC (carbon degassing rate).
#
###############################
from scipy import interpolate
from DependentVariable import *

class def_fC(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fC (carbon degassing rate).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        
        initialValue = variables.Ç.getValue(variables, time)
        presentValue = 1.0
        
        
        # Time before calcareous sea plankton appeared
        if  (time < -150):
            fC = initialValue
        # Time with calcareous sea plankton
        elif(time <= 0):
            # Linear growth
            f  = interpolate.interp1d([150, 0], [initialValue, presentValue], kind='linear')
            fC = f( abs(time) )
        
        
        values[index] = fC
#        values[index] = 1.0