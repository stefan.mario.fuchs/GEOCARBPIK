# -*- coding: utf-8 -*-

#############################
# Module:   def_C
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the state variable C (carbon in carbonates).
#
###############################
from StateVariable import *

class def_C(StateVariable):
    """ Description:
    
        Definition of the state variable C (carbon in carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        C  = values[index]
        dC = variables.dC.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index+1] = C + dC