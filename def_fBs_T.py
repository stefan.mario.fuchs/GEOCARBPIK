# -*- coding: utf-8 -*-

#############################
# Module:   def_fBs_T
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fBs_T (feedback factor 
#              expressing the dependence of weathering on temperature
#              for silicates).
#
###############################
import math
from DependentVariable import *

class def_fBs_T(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fBs_T (feedback factor 
        expressing the dependence of weathering on temperature
        for silicates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        ACT    = variables.ACT.getValue(variables,    time)
        TDiff  = variables.TDiff.getValue(variables,  time)
        RUNfBs = variables.RUNfBs.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = math.exp(ACT * TDiff) * math.pow(1 + RUNfBs * TDiff, 0.65)