# -*- coding: utf-8 -*-

#############################
# Module:   def_CO2
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable CO2 (amount of carbon 
#              dioxide in the atmosphere).
#
###############################
from DependentVariable import *

class def_CO2(DependentVariable):
    """ Description:
    
        Definition of the dependent variable CO2 (amount of carbon dioxid in
        the atmosphere).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        RCO2  = variables.RCO2.getValue(variables, time)
        CO2_0 = variables.CO2_0.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:        
        values[index] = RCO2 * CO2_0