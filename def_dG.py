# -*- coding: utf-8 -*-

#############################
# Module:   def_dG
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable dG (change rate of carbon 
#              in organic material).
#
###############################
from DependentVariable import *

class def_dG(DependentVariable):
    """ Description:
    
        Definition of the dependent variable dG (change rate of carbon in
        organic material).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:  
        Fbg = variables.Fbg.getValue(variables, time)
        Fwg = variables.Fwg.getValue(variables, time)
        Fmg = variables.Fmg.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = Fbg - (Fwg + Fmg)