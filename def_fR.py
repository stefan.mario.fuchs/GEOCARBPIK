# -*- coding: utf-8 -*-

#############################
# Module:   def_fR
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fR (ratio of average land 
#              elevation compared to the present value).
#
###############################

from DependentVariable import *

class def_fR(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fR (ratio of average land 
        elevation compared to the present value).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        L    = variables.L.getValue(variables, time)
        Rocm = variables.Rocm.getValue(variables, time)
        Roc  = variables.Roc.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:     
        values[index] = 1 - L * ((Roc - Rocm)/(Roc - 0.7))
#        values[index] = 1.0