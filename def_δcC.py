# -*- coding: utf-8 -*-

#############################
# Module:   def_δcC
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable δcC (δ13C value in 
#              carbonates).
#
###############################
from DependentVariable import *

class def_δcC(DependentVariable):
    """ Description:
    
        Definition of the dependent variable δcC (δ13C value in 
        carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        δc = variables.δc.getValue(variables, time)
        C  = variables.C.getValue(variables,  time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = δc * C