# -*- coding: utf-8 -*-

#############################
# Module:   SimulationSettings
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: All simulation settings are made on the right side (value) of 
#              the following 'settings' object. The left side (key) must not 
#              be changed.
#
###############################
import SimulationVariables

settings = {   
             
             # • Plot settings
             'plotResultsOf'        : ['RCO2','CO2'],                          # variables whose results should be plottet and saved in figures
             'startTime'            : -570,                                    # begin output at this time step
             'endTime'              : 0,                                       # end output at this time step
             'timeResolution'       : 10,                                      # resolution of the x-axis (needs to be 10 for reference curves)
             'valuesRange'          : None,                                    # [ymin, ymax]
             
             # • Result settings
             'loadResults'          : False,                                   # results for variables under 'plotResultsOf' will be loaded from the 'outputDirectory'
             'saveResultsOf'        : [],            # default: [] -> all      # variables whose results should be saved in CSV files
             
             # • File settings
             'inputDelimiter'       : '\t',                                    # specifies the delimiter that separates the values in the CSV input files
             'outputDelimiter'      : '\t',                                    # specifies the delimiter that separates the values in the CSV output files
             'inputDirectory'       : 'files/input/',                          # directory which contains CSV files for input variables
             'outputDirectory'      : 'files/output/',                         # directory for simulation results such as CSV files and figures
             'overwrite'            : True,                                    # 'False': all possible files in the 'outputDirectory' will be moved to a backup directory
    
             # • Time settings (in million years)
             'start'                : -570,          # default: -570           # simulation starting point
             'end'                  : 0,             # default: 0              # simulation ending point
             'stepSize'             : 1,             # default: 1              # ∆t = passing time between two simulation steps

             'BeforeVascularPlants' : -380,                                    # First appearance of vascular land plants
             'WithVascularPlants'   : -350,                                    # Vascular land plants were predominant
             'BeforeAngiosperms'    : -130,                                    # First appearance of angiosperms
             'WithAngiosperms'      :  -80,                                    # Angiosperms were predominant
             
           }
   


            
            
class Settings:
    
    # CONSTRUCTOR
    def __init__(self):
        
        # --------------------------------------------------------------------
        # TIME SETTINGS
        # --------------------------------------------------------------------
        start                      = settings['start']
        end                        = settings['end']
        stepSize                   = settings['stepSize']
        BeforeVascularPlants       = settings['BeforeVascularPlants']
        WithVascularPlants         = settings['WithVascularPlants']
        BeforeAngiosperms          = settings['BeforeAngiosperms']
        WithAngiosperms            = settings['WithAngiosperms']

        # • Time variable     
        t = []                              # t = [-570, -560, -550, ..., 0]                                    
        i = start                           # i = -570
        while i <= end:                     # i <= 0
            t.append(i)                     # t.append(-570)
            i  += stepSize                  # i += 10

        # • Duration
        duration = (len(t) -1) * stepSize
        
        # • Mathematical representation of i -> t[i]
        representation = {}                                     
        i = 0
        while i <= (duration/stepSize):     # i <= (570/10)
            representation[i] = t[i]        # 0 = -570, 1 = -560, 2 = -550, ...
            i += 1
        
        self._start                = start                                     # simulation starting point
        self._end                  = end                                       # simulation ending point
        self._stepSize             = stepSize                                  # ∆t = passing time between two simulation steps
        self._t                    = t                                         # time variable
        self._duration             = duration                                  # duration
        self._representation       = representation                            # mathematical representation of i -> t[i]
        self._beforeVascularPlants = BeforeVascularPlants                      # First appearance of vascular land plants
        self._withVascularPlants   = WithVascularPlants                        # Vascular land plants were predominant
        self._beforeAngiosperms    = BeforeAngiosperms                         # First appearance of angiosperms
        self._withAngiosperms      = WithAngiosperms                           # Angiosperms were predominant
        
        # --------------------------------------------------------------------
        # FILE SETTINGS
        # --------------------------------------------------------------------
        inputDelimiter            = settings['inputDelimiter']
        outputDelimiter           = settings['outputDelimiter']
        inputDirectory            = settings['inputDirectory']
        outputDirectory           = settings['outputDirectory']
        overwrite                 = settings['overwrite']

        # Ensures that every directory contains an ending slash
        if(outputDirectory[-1] != "/"):
            outputDirectory = outputDirectory + "/"
        if( inputDirectory[-1] != "/"):
            inputDirectory  = inputDirectory  + "/"
        
        self._inputDelimiter      = inputDelimiter
        self._outputDelimiter     = outputDelimiter
        self._inputDirectory      = inputDirectory
        self._outputDirectory     = outputDirectory
        self._overwrite           = overwrite
        
        # --------------------------------------------------------------------
        # RESULT SETTINGS
        # --------------------------------------------------------------------
        loadResults               = settings['loadResults']
        saveResultsOf             = settings['saveResultsOf']
        plotResultsOf             = settings['plotResultsOf']
        startTime                 = settings['startTime']
        endTime                   = settings['endTime']
        timeResolution            = settings['timeResolution']
        valuesRange               = settings['valuesRange']
        
        # plotResultsOf: [] -> assign all variables
        if(not plotResultsOf):
            for variable in SimulationVariables.variables:
                plotResultsOf.append(variable)
                saveResultsOf.append(variable)
        # saveResultsOf: [] -> assign all variables
        elif(not saveResultsOf):
            for variable in SimulationVariables.variables:
                saveResultsOf.append(variable)

        # Ensures that variables which should be plotted are saved first
        for variable in plotResultsOf:
            if(variable not in saveResultsOf):
                saveResultsOf.append(variable)        
                
        self._saveResultsOf       = saveResultsOf
        self._plotResultsOf       = plotResultsOf
        self._loadResults         = loadResults
        self._startTime           = startTime
        self._endTime             = endTime
        self._timeResolution      = timeResolution
        self._valuesRange         = valuesRange
    
    
    # GETTER
    def get_start(self):
        return self._start
    def get_end(self):
        return self._end
    def get_stepSize(self):
        return self._stepSize
    def get_duration(self):
        return self._duration
    def get_t(self):
        return self._t
    def get_beforeVascularPlants(self):
        return self._beforeVascularPlants
    def get_withVascularPlants(self):
        return self._withVascularPlants
    def get_beforeAngiosperms(self):
        return self._beforeAngiosperms
    def get_withAngiosperms(self):
        return self._withAngiosperms
    
    def get_inputDelimiter(self):
        return self._inputDelimiter
    def get_outputDelimiter(self):
        return self._outputDelimiter
    def get_inputDirectory(self):
        return self._inputDirectory
    def get_outputDirectory(self):
        return self._outputDirectory
    def get_overwrite(self):
        return self._overwrite
    
    def get_loadResults(self):
        return self._loadResults
    def get_saveResultsOf(self):
        return self._saveResultsOf
    def get_plotResultsOf(self):
        return self._plotResultsOf
    def get_startTime(self):
        return self._startTime
    def get_endTime(self):
        return self._endTime
    def get_timeResolution(self):
        return self._timeResolution
    def get_valuesRange(self):
        return self._valuesRange
    
    
    # PROPERTIES
    start                = property(get_start,                None, None, "'start' property.")
    end                  = property(get_end,                  None, None, "'end' property.")
    stepSize             = property(get_stepSize,             None, None, "'stepSize' property.")
    duration             = property(get_duration,             None, None, "'duration' property.")
    t                    = property(get_t,                    None, None, "'t' property.")
    beforeVascularPlants = property(get_beforeVascularPlants, None, None, "'beforeVascularPlants' property.")
    withVascularPlants   = property(get_withVascularPlants,   None, None, "'withVascularPlants' property.")
    beforeAngiosperms    = property(get_beforeAngiosperms,    None, None, "'beforeAngiosperms' property.")
    withAngiosperms      = property(get_withAngiosperms,      None, None, "'withAngiosperms' property.")
    
    inputDelimiter     = property(get_inputDelimiter,     None, None, "'inputDelimiter' property.")
    outputDelimiter    = property(get_outputDelimiter,    None, None, "'outputDelimiter' property.")
    inputDirectory     = property(get_inputDirectory,     None, None, "'inputDirectory' property.")
    outputDirectory    = property(get_outputDirectory,    None, None, "'outputDirectory' property.")
    overwrite          = property(get_overwrite,          None, None, "'overwrite' property.")
    
    loadResults        = property(get_loadResults,        None, None, "'loadResults' property.")
    saveResultsOf      = property(get_saveResultsOf,      None, None, "'saveResultsOf' property.")
    plotResultsOf      = property(get_plotResultsOf,      None, None, "'plotResultsOf' property.")
    startTime          = property(get_startTime,          None, None, "'startTime' property.")
    endTime            = property(get_endTime,            None, None, "'endTime' property.")
    timeResolution     = property(get_timeResolution,     None, None, "'timeResolution' property.")
    valuesRange        = property(get_valuesRange,        None, None, "'valuesRange' property.")

    
    # METHODS
    def getIndexByTime(self, time):   
        for i, t in self._representation.items():
            if(t == time):
                return i
    
    def getTimeByIndex(self, index):   
        for i, t in self._representation.items():
            if(i == index):
                return t