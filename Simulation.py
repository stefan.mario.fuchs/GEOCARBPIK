# -*- coding: utf-8 -*-

#############################
# Module:   Simulation
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: The simulation itself.
#
###############################

# ----------------------------------------------------------------------------
# RUN SIMULATION                     
# ---------------------------------------------------------------------------- 
def run(variables, settings):
    """ Description:
    
        Determines the values of all variables in each step.
        
    """
    t = settings.t
    
    i = 0
    while i <= (settings.duration/settings.stepSize):     # i <= (570/10)
#    while i <= 1:
        
        # DEBUG:
        print("# ---------------------------------------------------")
        print("Current time step: %i million years" % t[i])
        
        # Find steady state:
        findSteadyState(variables, settings, i)
            
        # Progress in time
        i  += 1          
         
    
        
# ----------------------------------------------------------------------------
# FIND STEADY STATE
# ---------------------------------------------------------------------------- 
def findSteadyState(variables, settings, i):
    """ Description:
    
        A state of equilibrium for carbon dioxide must be determined in each 
        time step. Therefore, all variables associated with it are recalculated 
        again and again until carbon dioxide converges.
        
    """    
    t = settings.t
    
    variables.RCO2.calculation(variables, t[i])