# -*- coding: utf-8 -*-

#############################
# Module:   def_Fbg
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fbg (burial rate of carbon 
#              as organic matter in sediments).
#
###############################
from DependentVariable import *

class def_Fbg(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fbg (burial rate of carbon as
        organic matter in sediments).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        δbc = variables.δbc.getValue(variables, time)
        αc  = variables.αc.getValue(variables,  time)
        Fwc = variables.Fwc.getValue(variables, time)
        Fmc = variables.Fmc.getValue(variables, time)
        Fwg = variables.Fwg.getValue(variables, time)
        Fmg = variables.Fmg.getValue(variables, time)
        δc  = variables.δc.getValue(variables,  time)
        δg  = variables.δg.getValue(variables,  time)
        
        # Perform the calculation and save the result in the variable:        
        values[index] = ( δbc*(Fwc + Fmc + Fwg + Fmg) - δc*(Fwc + Fmc) - δg*(Fwg + Fmg) ) / αc