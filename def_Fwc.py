﻿# -*- coding: utf-8 -*-

#############################
# Module:   def_Fwc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fwc (release of carbon 
#              into the atmosphere, oceans and biosphere through the weathering 
#              of carbonates).
#
###############################
from DependentVariable import *

class def_Fwc(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fwc (release of carbon 
        into the atmosphere, oceans and biosphere through the weathering 
        of carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        fBBc = variables.fBBc.getValue(variables, time)
        fLA  = variables.fLA.getValue(variables,  time)
        fAD  = variables.fAD.getValue(variables,  time)
        fE   = variables.fE.getValue(variables,   time)
        kWC  = variables.kWC.getValue(variables,  time)
        C    = variables.C.getValue(variables,    time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fBBc * fLA * fAD * fE * kWC * C