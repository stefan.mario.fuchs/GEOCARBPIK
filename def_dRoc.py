# -*- coding: utf-8 -*-

#############################
# Module:   def_dRoc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable dRoc (change rate of the 
#              average ratio of 87Sr/86Sr in oceans).
#
###############################
from DependentVariable import *

class def_dRoc(DependentVariable):
    """ Description:
    
        Definition of the dependent variable dRoc (change rate of the average
        ratio of 87Sr/86Sr in oceans).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        MSr  = variables.MSr.getValue(variables,  time)
        Roc  = variables.Roc.getValue(variables,  time)        
        Rbas = variables.Rbas.getValue(variables, time)
        Fbas = variables.Fbas.getValue(variables, time)
        Rriv = variables.Rriv.getValue(variables, time)
        Friv = variables.Friv.getValue(variables, time)

        # Perform the calculation and save the result in the variable:      
        values[index] = ( (Rbas - Roc) * Fbas + ( (Rriv - Roc) * Friv) ) / MSr