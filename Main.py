# -*- coding: utf-8 -*-

#############################
# Module:   Main
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: The main program loads all simulation settings and variables. 
#              It also starts the simulation and saves its results - or loads 
#              the results of a previous run - which can then be visualized.
#
###############################
import sys
import getopt

import Utilities
from   Utilities import CSV
from   Utilities import Directory
from   Utilities import Chart
import Simulation
import SimulationSettings
import SimulationVariables
# ----------------------------------------------------------------------------
# MAIN                     
# ----------------------------------------------------------------------------
def main(argv=None):
    """ Description:
        
        The main program loads all simulation settings and variables. It also 
        starts the simulation and saves its results - or loads the results of
        a previous run - which can then be visualized.
        
    """
    if argv is None:
        argv = sys.argv
    try:
        try:

# ----------------------------------------------------------------------------
# SIMULATION
            
            # LOAD SIMULATION SETTINGS 
            settings = SimulationSettings.Settings()
            # INITIALIZE SIMULATION VARIABLES               
            variables = SimulationVariables.Variables(settings)
            
            # SIMULATION RUN
            if(not settings.loadResults):    
                Simulation.run(variables, settings)
                saveResults(variables, settings)
   
            # PLOT RESULTS                     
            Chart.plotDevelopmentOverTime(variables, settings)
            
# ----------------------------------------------------------------------------
# ERROR HANDLING
                         
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
        except getopt.error as msg:
             raise Usage(msg)
        except FileNotFoundError as e:
             raise Error(e.args)
             
    except Usage as err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 2
    except Error as err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 1

        
        
# ----------------------------------------------------------------------------
# SAVE RESULTS
# ---------------------------------------------------------------------------- 
def saveResults(variables, settings):
    """ Description:
    
        Saves all results in CSV files.
        
    """
    overwrite       = settings.overwrite
    outputDirectory = settings.outputDirectory
    saveResultsOf   = settings.saveResultsOf
    
    # • Creates a backup of all files in the outputDirectory
    if(overwrite is False):
        if(not Utilities.Directory.empty(outputDirectory)):
            Utilities.Directory.backup(outputDirectory)
    
    # • Saves all data for each variable in saveResultsOf
    for variable in saveResultsOf:
        if(variable + '_reference' in SimulationVariables.variables):
            writeVariableToFile(variables, settings, variable + '_reference')
        writeVariableToFile(variables, settings, variable)
            
        
def writeVariableToFile(variables, settings, variable):
    outputDelimiter = settings.outputDelimiter
    outputDirectory = settings.outputDirectory
    t               = settings.t
    
    content = []
    i = 0
    while i <= (settings.duration/settings.stepSize):     # i <= (570/10)
        exec("content.append((t[i], variables.%s.getValue(variables, t[i])))" % (variable))        # -570, 5.0E+18
        i += 1
    
    file = outputDirectory + variable + '.csv'
    Utilities.CSV.write(content, outputDelimiter, file)
   

      
    
 # Usage() exception, which we catch in an except clause at the end of main()
class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

# Error() exception, used for expected errors like failure to open files
class Error(Exception):
    def __init__(self, msg):
        self.msg = msg

        


if __name__ == "__main__":
    main()