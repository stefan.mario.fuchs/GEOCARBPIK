# -*- coding: utf-8 -*-

#############################
# Module:   def_Rocm
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Rocm (measured average 
#              ratio of 87Sr/86Sr of seawater).
#
###############################
from DependentVariable import *

class def_Rocm(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Rocm (measured average
        ratio of 87Sr/86Sr of seawater).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        Sr   = variables.Sr.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = 0.7 + ( Sr/10000 )
#        values[index] = variables.Rocm_reference.getValue(variables, time)