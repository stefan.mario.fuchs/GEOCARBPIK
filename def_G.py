# -*- coding: utf-8 -*-

#############################
# Module:   def_G
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the state variable G (carbon in organic material).
#
###############################
from StateVariable import *

class def_G(StateVariable):
    """ Description:
    
        Definition of the state variable G (carbon in organic material).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        G  = values[index]
        dG = variables.dG.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index+1] = G + dG