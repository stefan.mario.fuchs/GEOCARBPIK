# -*- coding: utf-8 -*-

#############################
# Module:   def_fBBc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fBBc (feedback factor 
#              expressing the dependence of weathering on temperature and 
#              CO2 concentration for carbonates).
#
###############################
from DependentVariable import *

class def_fBBc(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fBBc (feedback factor expressing
        the dependence of weathering on the temperature and CO2 concentration 
        for carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:         
        fBBc_T = variables.fBBc_T.getValue(variables, time)
        f_CO2  = variables.f_CO2.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:        
        values[index] = fBBc_T * f_CO2