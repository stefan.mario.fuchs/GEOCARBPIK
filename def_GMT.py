# -*- coding: utf-8 -*-

#############################
# Module:   def_GMT
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable GMT (global mean surface 
#              air temperature).
#
###############################
import math
from DependentVariable import *

class def_GMT(DependentVariable):
    """ Description:
    
        Definition of the dependent variable GMT (global mean surface 
        air temperature).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step: 
        τ    = variables.τ.getValue(variables,    time)
        RCO2 = variables.RCO2.getValue(variables, time)
        Ws   = variables.Ws.getValue(variables,   time)
        
        # Perform the calculation and save the result in the variable:        
        values[index] = 15 + (τ * math.log(RCO2)) - (Ws * (abs(time)/570))