# -*- coding: utf-8 -*-

#############################
# Module:   def_fBs
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fBs (feedback factor 
#              expressing the dependence of weathering on temperature and 
#              CO2 concentration for silicates).
#
###############################
import math
from DependentVariable import *

class def_fBs(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fBs (feedback factor expressing
        the dependence of weathering on temperature and CO2 concentration for
        silicates).
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        Fwsi   = variables.Fwsi.getValue(variables,   time)
        fR     = variables.fR.getValue(variables,     time)
        fE     = variables.fE.getValue(variables,     time)
        fAD    = variables.fAD.getValue(variables,    time)
        Fwsi_0 = variables.Fwsi_0.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = Fwsi / ( fR * fE * math.pow(fAD, 0.65) * Fwsi_0 )