# -*- coding: utf-8 -*-

#############################
# Module:   def_fBBc_T
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fBBc_T (feedback factor 
#              expressing the dependence of weathering on temperature for 
#              carbonates).
#
###############################
from DependentVariable import *

class def_fBBc_T(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fBBc_T (feedback factor 
        expressing the dependence of weathering on temperature for 
        carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        RUNfBBc = variables.RUNfBBc.getValue(variables, time)
        TDiff   = variables.TDiff.getValue(variables,  time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = 1 + RUNfBBc * TDiff