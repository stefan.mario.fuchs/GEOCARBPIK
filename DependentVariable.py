# -*- coding: utf-8 -*-

#############################
# Module:   DependentVariable
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This class contains attributes and methods that all dependent
#              variables have in common. Special dependent variables (such as 
#              fBs and fBBc) are derived from this class.
#
###############################
from SimulationVariable import *

class DependentVariable(SimulationVariable):
    """ Description:
    
        This class contains attributes and methods that all dependent
        variables have in common. Special dependent variables (such as fBs
        and fBBc) are derived from this class.
        
    """
    # CONSTRUCTOR
    def __init__(self, settings, args=None):
        super().__init__(settings, args)
        
        values     = self._values
        startValue = self._args
        
        # Initialize the variable:
        values.append(startValue)                    # for i = -570
        i = (settings.start + settings.stepSize)     # i = -569 (not -570, because the first value is given)
        while i <= settings.end:                     # i <= 0
            values.append(None)
            i  += settings.stepSize                  # i += 1
        
        self._values = values

        
    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def getValue(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # If the variable has no value for the time step...
        if(values[index] is None):
            # ...start the calculation:
            self.calculation(variables, time)
        
        return values[index]

    def calculation(self, variables, time):
        ...