# -*- coding: utf-8 -*-

#############################
# Module:   CSV
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Imports and exports data from/to CSV files.
#
###############################
""" Description:
    
    Imports and exports data from/to CSV files.
    
"""

import os

import Utilities
from Utilities import Convert

def read(location, separator):
    """ Description:
    
        Opens a CSV file and returns its contents.
        
    """
    try:
        content = []
    
        file = open(location, "r")
        for line in file.readlines():
            # removes whitespace from the beginning or end
            line = line.strip()
            # splits the string into a list
            line = line.split(separator)
    #        print(line) # for debugging purposes
            # None values will be ignored
            if('None' not in line):
                content.append(line)
        file.close()
        
        return content
        
    except:
        file.close()
        return None

def write(content, separator, location):
    """ Description:
    
        Exports table data to a CSV file.
        
    """
    try:
        file = open(location, "w")
        formattedLine = ""
        for line in content:
            # inserts a return if its not the first entry
            if(formattedLine != ""):
                formattedLine = "\n"
            # separates the entries
            for entry in line:
                if(formattedLine != "" and formattedLine != "\n"):
                    formattedLine = formattedLine + separator
                formattedLine = formattedLine + str(entry)
#                print(formattedLine) # for debugging purposes
            file.writelines(formattedLine)
        file.close()
    
    except:
        file.close()
        return None

    
def convertContent(content):
    """ Description:
    
        When a table is exported to a CSV file, all data is saved as a string
        so that the information about the original data type is lost. Based on 
        the contents of a cell, this method recognizes a data type and restores 
        it.
        
    """
    result = []

    for line in content:
        newLine = []
        for entry in line:
            entry = Utilities.Convert.auto(entry)
            newLine.append(entry)
        result.append(newLine)
    
    return result