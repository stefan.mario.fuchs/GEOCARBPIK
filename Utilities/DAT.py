# -*- coding: utf-8 -*-

#############################
# Module:   DAT
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Imports proxy files in a specific format.
#
###############################
""" Description:
    
    Imports proxy files in a specific format.
    
"""

import os

def readProxyFile(location, separator):
    """ Description:
    
        Opens a proxy file and returns its contents.
        
    """
    try:
        content = []
    
        file = open(location, "r")
        for line in file.readlines():
            # removes whitespace from the beginning or end
            line = line.strip()
            # removes dublicate spaces
            line = " ".join(line.split())
            # empty lines and comments will be ignored
            if('#' not in line and line != ''):
                # splits the string into a list
                line = line.split(separator)
                content.append(line)
        file.close()
        
        return content
        
    except:
        file.close()
        return None