# -*- coding: utf-8 -*-

#############################
# Module:   Table
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This module generates charts for the results.
#
###############################
""" Description:
    
    This module generates charts for the results.
    
"""
import numpy as numPy
import scipy as sciPy
import matplotlib as mpl
import matplotlib.pyplot as plt

import Utilities
from   Utilities import DAT
from   Utilities import Convert
import SimulationVariables

def plotDevelopmentOverTime(variables, settings):
    """ Description:
    
        Visualizes the development over time for each variable.
        
    """
    plotResultsOf   = settings.plotResultsOf
    outputDelimiter = settings.outputDelimiter
    outputDirectory = settings.outputDirectory
    
    # General settings for all figures
    # • figure size -> figureSize(width, height)
    figureSize = (10, 5)
    # • font style for title and axis
    titleFont = {'fontname':'Arial', 'size':'24', 'color':'black', 'weight':'normal', 'verticalalignment':'bottom'}
    axisFont  = {'fontname':'Arial', 'size':'14'}
    mpl.rc('font',family='Arial')
    # • range and interval of x-axis
    xmin     = settings.startTime
    xmax     = settings.endTime
    # • range of y-axis
    valuesRange = settings.valuesRange
    
    # Plots all data for each variable in plotResultsOf
    for variable in plotResultsOf:
        
        # Apply general settings
        figure, graph = plt.subplots(figsize=figureSize)
#        plt.title(variable + " over time", **titleFont)
        plt.xlabel('Time (Ma)'  , **axisFont)
        plt.ylabel(variable, **axisFont)
        
        
        
        
            
        # Open CSV file with results
        fileContent = Utilities.CSV.read(outputDirectory + variable + '.csv', outputDelimiter)
        fileContent = Utilities.CSV.convertContent(fileContent)
        
        (x, y) = getXY(settings, fileContent, 0, 1)
        
        # Get value range for scale
        ymax = max(y) + 0.1 * abs(max(y) - min(y))
        ymin = min(y) - 0.1 * abs(max(y) - min(y))
        
        
        
        # Assign values to plot
        graph.plot(x, y, linestyle='-', color='darkblue', label='GEOCARB$^{PIK}$', zorder=2)
            
        if(variable == 'RCO2'):
            
#            plt.title(r'Relative $CO_2$ concentration in the atmosphere', **titleFont)
            plt.ylabel(r'RCO$_2$', **axisFont)
            
            # Horizontal line at value 1.0
            graph.hlines(1.0, xmin, xmax, colors='grey', linestyles='dashed', label='Present value', zorder=3) 
                
        # If applicable, insert proxy data
        if(variable == 'CO2'):
            
#            plt.title(r'Absolute $CO_2$ concentration in the atmosphere', **titleFont)
            plt.ylabel(r'CO$_2$ (ppm)', **axisFont)
        
            # Read file content
            fileContent = Utilities.DAT.readProxyFile(settings.inputDirectory + 'CO2_proxies.dat', ' ')
            
            # Specify columns with x and y values
            xColumns = [0, 2]
            yColumns = [3, 5]
            (xCollection, yCollection) = getXYCollections(fileContent, xColumns, yColumns)
            
            (x_proxy, xMargin)         = analyseValues(xCollection)
            (y_proxy, yMargin)         = analyseValues(yCollection)
            
            # Insert proxy data into graph
            graph.errorbar(x_proxy, y_proxy, xerr=xMargin, yerr=yMargin, fmt='o', label='Proxies', zorder=1)
            
            # Horizontal line at value 300
            graph.hlines(300, xmin, xmax, colors='grey', linestyles='dashed', label='Present value', zorder=3) 
            
        # If applicable, insert a reference curve
        if(variable + '_reference' in SimulationVariables.variables):
            
            # Open CSV file with results
            fileContent = Utilities.CSV.read(outputDirectory + variable + '_reference.csv', outputDelimiter)
            fileContent = Utilities.CSV.convertContent(fileContent)
            
            (x_ref, y_ref) = getXY(settings, fileContent, 0, 1)

            # Get value range for scale
            if(max(y_ref) > max(y)):
                ymax = max(y_ref) + 0.1 * abs(max(y_ref) - min(y_ref))
            if(min(y_ref) < min(y)):
                ymin = min(y_ref) - 0.1 * abs(max(y_ref) - min(y_ref))
            
            # Assign values to plot
            graph.plot(x_ref, y_ref, marker='.', linestyle=':', label="Reference", zorder=1) 
        
        
        
        # Set scale
        graph.axis([xmin, xmax, ymin, ymax])
        if(settings.valuesRange is not None):
            graph.axis([xmin, xmax, valuesRange[0], valuesRange[1]])
        
        # Add a legend (loc = position)
        plt.legend(fontsize=10, loc=1, handlelength=3.5)
        
        # Plot
#        plt.savefig(outputDirectory + variable + '.png', dpi=300)
        plt.savefig(outputDirectory + variable + '.pdf', format='pdf', dpi=600, bbox_inches='tight')
        plt.show() # for debugging purposes
        


def getXY(settings, fileContent, xColumn, yColumn):
    """ Description:
    
        Identifies the x- and y-values in a file.
        
    """
    
    interval = settings.timeResolution
    
    xValues = []
    yValues = []
    
    for line in fileContent:
        time = line[xColumn]
        if(int(time/interval) == time/interval):
            xValues.append(line[xColumn])
            yValues.append(line[yColumn])
    
    return xValues, yValues


    
def getXYCollections(fileContent, xColumns, yColumns):
    """ Description:
    
        Identifies the x- and y-values in a file.
        
    """
    
    # Necessary expansion of the range by 1
    xColumns = range(xColumns[0], xColumns[1] +1)
    yColumns = range(yColumns[0], yColumns[1] +1)

    xCollection = []
    yCollection = []

    for line in fileContent:
        
        xValues = []
        yValues = []

        i = 0        
        for data in line:
            if(  i in xColumns):
                data = Convert.auto(data)
                xValues.append(-data)
            elif(i in yColumns):
                data = Convert.auto(data)
                yValues.append(data)
            i += 1
        
        xCollection.append(xValues)
        yCollection.append(yValues)
    
    return xCollection, yCollection
          
    
    
def analyseValues(collection):
    """ Description:
    
        Determines values and their possible deviations from a collection.
        
    """
    values = []
    deviations = [[],[]] # deviation = [left, right]
    
    for data in collection:
        # Only one value is given
        if( -1 in data ):
            data.remove(-1)
            lowerValue = data[0]
            upperValue = data[0]
        # Three values are given
        else:
            lowerValue = min(data)
            upperValue = max(data)
            data.remove(lowerValue)
            data.remove(upperValue)
        
        value = data[0]
        
        values.append(value)
        left  = lowerValue - value
        right = value - upperValue
        deviations[0].append(left)
        deviations[1].append(right)
    
    return values, deviations