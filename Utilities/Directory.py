# -*- coding: utf-8 -*-

#############################
# Module:   Directory
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This module handles all directory operations (like creating a 
#              directory or a backup of a directory, etc.)
#
###############################
""" Description:
    
    This module handles all directory operations (like creating a 
    directory or a backup of a directory, etc.)
    
"""
import os
import shutil
import time

def empty(location):
    """ Description:
    
        Checks if the directory is empty.
        
    """
    # If a directory does not exist, it will be created
    if(not os.path.exists(location)):
        os.mkdir(location)
    
    listOfFiles = os.listdir(location)
    if len(listOfFiles) > 0:
        return False
    return True
    


def backup(source):
    """ Description:
    
        Creates a backup of the source directory.
        
    """
    timestamp = time.strftime("%Y.%m.%d_%H%M%S", time.localtime())
    destination = source[0:-1] + "_" + str(timestamp) + "/"
    move(source, destination)



def move(source, destination):
    """ Description:
    
        Moves all files from the source directory to the target directory.
        
    """
    os.mkdir(destination)
    listOfFiles = os.listdir(source)
    for file in listOfFiles:
        if((source + file) != (destination + file)):
            shutil.move(source + file, destination + file)