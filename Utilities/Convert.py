# -*- coding: utf-8 -*-

#############################
# Module:   Convert
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Converts a string to a more suitable format.
#
###############################
""" Description:
    
    Converts a string to a more suitable format.
    
"""

def boolean(string):
    """ Description:
    
        If a string corresponds to the logical values 'True'/'False',
        its value is returned, otherwise an error message.
    
    """
    if string == 'True':
        return True
    if string == 'False':
        return False
    raise ValueError("Error: Not a boolean value.")
    
def auto(string):
    """ Description:
    
        Detects the data type (boolean, int, float, string) of a string and
        converts them into the corresponding format.
    
    """
    for dataType in (boolean, int, float):
        try:
            return dataType(string) # type cast
        except ValueError:
            pass
    return string # return as string