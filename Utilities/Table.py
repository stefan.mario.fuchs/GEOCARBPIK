# -*- coding: utf-8 -*-

#############################
# Module:   Table
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This module contains methods especially for table manipulation.
#
###############################
""" Description:
    
    This module contains methods especially for table manipulation.
    
"""

def findCell(table, entry):
    """ Description:
    
        Searches for a value within a table and returns its cell (row, column).
        
    """
    for row, column in enumerate(table):
        try:
            return (row, column.index(entry))
        except ValueError:
            pass
    
    return None