# -*- coding: utf-8 -*-

#############################
# Module:   def_dδcC
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable dδcC (change of the 
#              isotope ratio of δ13C in carbonates).
#
###############################
from DependentVariable import *

class def_dδcC(DependentVariable):
    """ Description:
    
        Definition of the dependent variable dδcC (change of the isotope
        ratio of δ13C in carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        δbc = variables.δbc.getValue(variables, time)
        δc  = variables.δc.getValue(variables,  time)
        Fbc = variables.Fbc.getValue(variables, time)
        Fwc = variables.Fwc.getValue(variables, time)
        Fmc = variables.Fmc.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:        
        values[index] = δbc*Fbc - δc*(Fwc + Fmc)