# -*- coding: utf-8 -*-

#############################
# Module:   def_Roc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the state variable Roc (average ratio of 87Sr/86Sr 
#              in the oceans).
#
###############################
from StateVariable import *

class def_Roc(StateVariable):
    """ Description:
    
        Definition of the state variable Roc (average ratio of 87Sr/86Sr 
        in the oceans).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        Roc  = values[index]
        dRoc = variables.dRoc.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:        
        values[index+1] = Roc + dRoc
#        values[index+1] = variables.Roc_reference.getValue(variables, time+settings.stepSize)