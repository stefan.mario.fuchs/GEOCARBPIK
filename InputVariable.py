# -*- coding: utf-8 -*-

#############################
# Module:   InputVariable
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This class contains attributes and methods that all input 
#              variables have in common. Special input variables (such as fA, 
#              fD and fL) are derived from this class.
#
###############################
import os
from Utilities import CSV, Table
from SimulationVariable import *

class InputVariable(SimulationVariable):
    """ Description:
    
        This class contains attributes and methods that all input variables
        have in common. Special input variables (such as fA, fD and fL) are
        derived from this class.
        
    """
    # CONSTRUCTOR
    def __init__(self, settings, args=None):
        super().__init__(settings, args)
        
        values = self._values
        assignment = self._args
        
        # Determines if assignment is a file 
        isFile = False
        if(type(assignment) is str):
            isFile = os.path.exists(settings.inputDirectory + assignment)
        
        # If the assignment is a file...
        if(isFile):
            # ...then the file is opened and read...
            csvFile = Utilities.CSV.read(settings.inputDirectory + args, settings.inputDelimiter)
            content = Utilities.CSV.convertContent(csvFile)
            
            # ...and the values are assigned to the corresponding time steps:
            i = 0
            while i <= (settings.duration/settings.stepSize):     # i <= (570/10)
                row = Utilities.Table.findCell(content, settings.t[i])[0]
                column = 1
                if(row != None):
                     value = content[row][column]
                     self._values.append(value)   
                i  += 1
        
        # If the assignment is a fixed value...
        else:
            # ... then this value is set for all time steps:
            i = 0
            while i <= (settings.duration/settings.stepSize):
                self._values.append(args)
                i  += 1            
            
        self._values = values

                
    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def getValue(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        return values[index]