# -*- coding: utf-8 -*-

#############################
# Module:   def_Fwsi
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fwsi (absorption of CO2 
#              through weathering of Ca and Mg silicates, followed by 
#              precipitation of Ca and Mg as carbonates).
#
###############################
from DependentVariable import *

class def_Fwsi(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fwsi (absorption of CO2 
        through weathering of Ca and Mg silicates, followed by 
        precipitation of Ca and Mg as carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        Fbc = variables.Fbc.getValue(variables, time)        
        Fwc = variables.Fwc.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = Fbc - Fwc