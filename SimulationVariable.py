# -*- coding: utf-8 -*-

#############################
# Module:   SimulationVariable
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This class contains attributes and methods that all simulation  
#              variables have in common. Special variables (such as input 
#              variables, dependent variables and state variables) are derived
#              from this class.
#
###############################
import Utilities

class SimulationVariable:
    """ Description:
        
        This class contains attributes and methods that all simulation 
        variables have in common. Special variables (such as input variables,
        dependent variables and state variables) are derived from this class.
            
    """
    # CONSTRUCTOR
    def __init__(self, settings, args=None):
        self._values   = []
        self._settings = settings
        self._args     = args

        
    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def getValue(self, variables, time):
        ...