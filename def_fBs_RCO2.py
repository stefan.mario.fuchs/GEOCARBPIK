# -*- coding: utf-8 -*-

#############################
# Module:   def_fBs_RCO2
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fBs_RCO2 (feedback factor 
#              expressing the dependence of weathering on CO2 concentration
#              for silicates).
#
###############################
from DependentVariable import *

class def_fBs_RCO2(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fBs_RCO2 (feedback factor 
        expressing the dependence of weathering on CO2 concentration
        for silicates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step: 
        fBs_T = variables.fBs_T.getValue(variables,  time)
        f_CO2 = variables.f_CO2.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fBs_T * f_CO2