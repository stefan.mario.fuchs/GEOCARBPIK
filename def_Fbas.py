# -*- coding: utf-8 -*-

#############################
# Module:   def_Fbas
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fbas (Sr transfer between 
#              basalt and seawater).
#
###############################
from DependentVariable import *

class def_Fbas(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fbas (Sr transfer between basalt
        and seawater).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        fSR    = variables.fSR.getValue(variables,    time)
        Fbas_0 = variables.Fbas_0.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fSR * Fbas_0