﻿# -*- coding: utf-8 -*-

#############################
# Module:   def_Fwg
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable Fwg (release of carbon 
#              into the atmosphere, oceans and biosphere through the weathering 
#              of organic material).
#
###############################
from DependentVariable import *

class def_Fwg(DependentVariable):
    """ Description:
    
        Definition of the dependent variable Fwg (release of carbon 
        into the atmosphere, oceans and biosphere through the weathering 
        of organic material).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        fR  = variables.fR.getValue(variables,  time)
        fAD = variables.fAD.getValue(variables, time)
        kWG = variables.kWG.getValue(variables, time)
        G   = variables.G.getValue(variables,   time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = fR * fAD * kWG * G