# -*- coding: utf-8 -*-

#############################
# Module:   def_δgG
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable δgG (δ13C value in 
#              organic material).
#
###############################
from DependentVariable import *

class def_δgG(DependentVariable):
    """ Description:
    
        Definition of the dependent variable δgG (δ13C value in 
        organic material).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        δg = variables.δg.getValue(variables, time)
        G  = variables.G.getValue(variables,  time)
        
        # Perform the calculation and save the result in the variable:
        values[index] = δg * G