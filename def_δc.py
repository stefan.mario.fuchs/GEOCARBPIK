# -*- coding: utf-8 -*-

#############################
# Module:   def_δc
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the state variable δc (δ13C ratio in carbonates).
#
###############################
from StateVariable import *

class def_δc(StateVariable):
    """ Description:
    
        Definition of the state variable δc (δ13C ratio in carbonates).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        δcC  = variables.δcC.getValue(variables,  time)
        dδcC = variables.dδcC.getValue(variables, time)
        C    = variables.C.getValue(variables,   (time + settings.stepSize) )
        
        # Perform the calculation and save the result in the variable:
        values[index+1] = (δcC + dδcC) / C