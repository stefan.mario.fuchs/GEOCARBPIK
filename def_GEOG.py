# -*- coding: utf-8 -*-

#############################
# Module:   def_GEOG
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable GEOG (influence of changes 
#              in paleogeography on temperature).
#
###############################
from DependentVariable import *

class def_GEOG(DependentVariable):
    """ Description:
    
        Definition of the dependent variable GEOG (influence of changes 
        in paleogeography on temperature).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:
        T   = variables.T.getValue(variables,   time)
        T_0 = variables.T_0.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable: 
        values[index] = T - T_0