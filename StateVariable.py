# -*- coding: utf-8 -*-

#############################
# Module:   StateVariable
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: This class contains attributes and methods that all state 
#              variables have in common. Special state variables (such as C, G
#              and δcC) are derived from this class.
#
###############################
from DependentVariable import *

class StateVariable(DependentVariable):
    """ Description:
    
        This class contains attributes and methods that all state variables
        have in common. Special state variables (such as C, G and δcC) are 
        derived from this class.
        
    """
    # CONSTRUCTOR
    ...

        
    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def getValue(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # If the variable has no value for the time step...
        if(values[index] is None):
            # ...start the calculation:
            self.calculation(variables, (time - settings.stepSize)) # State variables are dependent on their previous state
        
        return values[index]