# -*- coding: utf-8 -*-

#############################
# Module:   def_fAD
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable fAD (ratio of the runoff
#              compared to the present value).
#
###############################
from DependentVariable import *

class def_fAD(DependentVariable):
    """ Description:
    
        Definition of the dependent variable fAD (ratio of the runoff compared
        to the present value).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        # Query the values of the necessary variables for the time step:        
        fA = variables.fA.getValue(variables, time)
        fD = variables.fD.getValue(variables, time)
        
        # Perform the calculation and save the result in the variable:          
        values[index] = fA * fD