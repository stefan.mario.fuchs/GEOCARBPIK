# -*- coding: utf-8 -*-

#############################
# Module:   def_f_CO2
# Author:   Stefan M. Fuchs
# Date:     September 2016
# Version:  1.0
#
# Description: Definition of the dependent variable f_CO2 (feedback factor 
#              expressing the dependence of weathering on CO2 levels).
#
###############################
import math
from DependentVariable import *

class def_f_CO2(DependentVariable):
    """ Description:
    
        Definition of the dependent variable f_CO2 (feedback factor expressing
        the dependence of weathering on CO2 levels).
        
    """
    # CONSTRUCTOR
    ...

    # GETTER
    ...

    
    # SETTER
    ...

    
    # DELETER
    ...
    
    
    # PROPERTIES
    ...
    
    
    # METHODS
    def calculation(self, variables, time):
        values   = self._values
        settings = self._settings
        index    = settings.getIndexByTime(time)
        
        beforeVascularPlants = settings.beforeVascularPlants
        withVascularPlants   = settings.withVascularPlants
        
        # Depending on the stage of plant development, the CO2 concentration 
        # has a slightly different effect on weathering:
        if  (time < beforeVascularPlants):
            f_CO2 = self.a(variables, time)
        elif(time >= beforeVascularPlants and time < withVascularPlants):
            f_CO2 = self.b(variables, time, abs(beforeVascularPlants), abs(withVascularPlants)+1 )
        if(time >= withVascularPlants and time <=   0):
            f_CO2 = self.c(variables, time)
        
        values[index] = f_CO2




    def a(self, variables, time):
        RCO2 = variables.RCO2.getValue(variables, time)
        
        f_CO2 = math.pow(RCO2, 0.50)
        
        return f_CO2
   
        
        
    def b(self, variables, time, begin, end):
        begin    = abs(begin)
        end      = abs(end)
        duration = begin - end
        
        a = self.a(variables, time)
        c = self.c(variables, time)
        
        time    = abs(time)
        weightA = abs(time-end ) /duration
        weightC = abs(time-begin)/duration
        
        f_CO2 = weightA * a + weightC * c
        
        return f_CO2

        
        
    def c(self, variables, time):
        RCO2 = variables.RCO2.getValue(variables, time)
        FERT = variables.FERT.getValue(variables, time)
        
        f_CO2 = math.pow( (2 * RCO2/(1 + RCO2)), FERT)
        
        return f_CO2